from kubernetes.client import models as k8s
from airflow.configuration import conf

def pod_mutation_hook(pod: k8s.V1Pod):
    extra_labels = {
        "kubernetes_executor": "False",
        "kubernetes_pod_operator": "False"
    }

    if 'airflow-worker' in pod.metadata.labels.keys() or \
            conf.get('core', 'EXECUTOR') == "KubernetesExecutor":
        extra_labels["kubernetes_executor"] = "True"
    else:
        extra_labels["kubernetes_pod_operator"] = "True"

    pod.metadata.labels.update(extra_labels)

